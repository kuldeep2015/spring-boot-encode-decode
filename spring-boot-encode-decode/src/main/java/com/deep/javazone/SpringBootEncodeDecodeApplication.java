package com.deep.javazone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootEncodeDecodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootEncodeDecodeApplication.class, args);
	}

}
