package general.csv;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.springframework.util.ResourceUtils;

public class CsvDataToBase64 {
	
	//private static String filePath = "classpath:CITY.csv";
	private static String inputFileName = "classpath:CITY.csv";
	private static String outputFileName = "output.csv";
	
	
	public static void main(String[] args) throws FileNotFoundException {
		CsvDataToBase64 csvDataToBase64 = new CsvDataToBase64();
		//String fileName = filePath + "/" + inputFileName;
		String fileContentEncoded = csvDataToBase64.convertFileContentToBase64Encode(inputFileName);
		System.out.println("File Encoded Content:- " + fileContentEncoded);
		csvDataToBase64.convertFileContentToBase64Decode(fileContentEncoded);
		
		
	}
	
	
	private String convertFileContentToBase64Encode(String fileName) throws FileNotFoundException{
		
		//File file = new File(fileName);
		File file = ResourceUtils.getFile(fileName);
		FileInputStream documentInFile = null;
		String fileContentEncoded = null;
		try {           
		    // Read file
		    documentInFile = new FileInputStream(file);
		    byte documentData[] = new byte[(int) file.length()];
		    documentInFile.read(documentData);

		    // Convert bytes array to Base64 string
		    fileContentEncoded = new String(Base64.encodeBase64(documentData));
		    System.out.println("+++ File converted "+fileName);
		} catch (FileNotFoundException e) {
			System.out.println("*** File not found "+fileName+"\n"+e);
		} catch (IOException ioe) {
			System.out.println("*** Error converting file "+fileName+"\n"+ioe);
		} finally {
			try {
				// Close  file after conversion
				if (documentInFile != null) {
					documentInFile.close();
					//file.delete();
				}
			} catch (IOException e) {
				System.out.println("*** Error for file "+fileName+"\n"+e);
				e.printStackTrace();
			}
		}
		
		return fileContentEncoded;
	}
	
	private void convertFileContentToBase64Decode(String encodedContent){
		FileOutputStream fileOuputStream = null;
		byte[] decodedBytes = Base64.decodeBase64(encodedContent.getBytes());
		String decodedContrnt = new String (decodedBytes);
		System.out.println("Decoded Content:- " + decodedContrnt);
        try {
            fileOuputStream = new FileOutputStream(outputFileName);
            fileOuputStream.write(decodedBytes);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOuputStream != null) {
                try {
                    fileOuputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}
	
}
